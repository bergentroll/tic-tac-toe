# !/usr/bin/env python3
# coding: utf-8

import os
import time
import random

grid = [['  ' for i in range(9)] for i in range(9)]
# Индикатор, определяющий, чей ход. Работает как мигалка,
# в случае победы принимает значение 0
turn = 1
delay = 0.2


def clrscrn():
    if os.name in ('posix', 'mac'):
        os.system('clear')
    elif os.name == 'nt':
        os.system('clr')
    else:
        print('___________')

# Заполняет сетку
for i in range(9):
    for j in range(9):
        if (j + 1) % 3 == 0 and j < 8:
            grid[i][j] = ' |'
        elif (i + 1) % 3 == 0 and i < 8:
            grid[i][j] = '__'
        if (j + 1) % 3 == (i + 1) % 3 == 0 and j < 8 and i < 8:
            grid[i][j] = '_|'


# Очищает поле
def clear():
    global field
    time.sleep(delay * 4)
    field = [[0 for i in range(3)] for i in range(3)]


# Рисует сетку на экране
def draw():
    clrscrn()
    for row in grid:
        print(''.join([str(elem) for elem in row]))


# Переводит значения полей на сетку
def field2grid():
    for i in range(3):
        for j in range(3):
            if field[i][j] == 0:
                grid[3 * (i + 1) - 2][3 * (j + 1) - 2] = '  '
            if field[i][j] == 1:
                grid[3 * (i + 1) - 2][3 * (j + 1) - 2] = 'X '
            if field[i][j] == -1:
                grid[3 * (i + 1) - 2][3 * (j + 1) - 2] = 'O '
    draw()


# Ищет пустую клетку
def empty():
    empty_list = []
    for i in range(3):
        for j in range(3):
            if field[i][j] == 0:
                empty_list.append([i, j])
    if len(empty_list) < 8 and field[1][1] == 0:
        empty_list = [[1, 1]]
    return empty_list


def intercept(turn, location, ind):
    print('___________')
    print('Position: ', location, '.', sep='')
    if location == 'row':
        field[ind][field[ind].index(0)] = turn
        return turn * -1
    elif location == 'col':
        for i in range(3):
            if field[i][ind] == 0:
                field[i][ind] = turn
                return turn * -1
        return 0
    elif location == 'slash':
        field[ind][ind] = turn
        return turn * -1
    elif location == 'backslash':
        field[ind][2 - ind] = turn
        return turn * -1

    elif location == 'random':
        empty_list = empty()
        if empty_list == []:
            clear()
        else:
            rnd = random.randint(0, len(empty_list) - 1)
            i = empty_list[rnd][0]
            j = empty_list[rnd][1]
            if field[i][j] == 0:
                field[i][j] = turn
        return turn * -1


# Перебор вариантов хода
def ai(turn):

    threat = 0
    location = ''
    ind = -1

    for i in range(3):
        col_sum = sum([field[j][i] for j in range(3)])
        row_sum = sum(field[i])
        if 1 < abs(col_sum) > threat:
            threat = abs(col_sum)
            location = 'col'
            ind = i
        if 1 < abs(row_sum) > threat:
            threat = abs(row_sum)
            location = 'row'
            ind = i

        if threat < -(turn * col_sum) == 1:
            for row in field:
                if row[i] == 0:
                    threat = abs(col_sum)
                    location = 'col'
                    ind = i
        if threat < -(turn * row_sum) == 1 and field[i].count(0) > 0:
            threat = abs(row_sum)
            location = 'row'
            ind = i

    slash = sum([field[i][i] for i in range(3)])
    if abs(slash) >= threat:
        for i in range(3):
            if field[i][i] == 0:
                threat = abs(slash)
                location = 'slash'
                ind = i
                if i == 1:
                    break
    backslash = sum([field[i][2 - i] for i in range(3)])
    if abs(slash) < abs(backslash) >= threat:
        for i in range(3):
            if field[i][2 - i] == 0:
                threat = abs(backslash)
                location = 'backslash'
                ind = i
                if i == 1:
                    break

    if threat >= 3:
        return 0
    elif 0 < threat < 3:
        return intercept(turn, location, ind)

    else:
        return intercept(turn, 'random', 0)

clear()

while turn != 0:
    field2grid()
    turn = ai(turn)
    time.sleep(delay)
